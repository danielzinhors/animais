import ScroolSuave from "./modules/scroll-suave.js";
import AccordionFaq from "./modules/accordion.js";
import TabMenu from "./modules/tabnav.js";
import Modal from "./modules/modal.js";
import Tooltip from "./modules/tooltip.js";
import DropdownMenu from "./modules/dropdown-menu.js";
import MenuMobile from "./modules/menu-mobile.js";
import initAnimaisApi from "./modules/animais-api.js";
import Funcionamento from "./modules/funcionamento.js";
import initBtcDoacao from "./modules/btc-doacao.js";
import AnimacaoScroll from "./modules/scroll-anime.js";
import SlideNav from "./modules/slide-custom-controls.js";

const scrollSuave = new ScroolSuave('[data-anime="menu"] a[href^="#"]');
scrollSuave.init();

const accordionFaq = new AccordionFaq("[data-anime='accordion'] dt");
accordionFaq.init();

const tabMenu = new TabMenu(
  "[data-tab='menu'] li",
  "[data-tab='content'] section"
);
tabMenu.init();
//
const modal = new Modal(
  '[data-modal="abrir"]',
  '[data-modal="fechar"]',
  '[data-modal="container"]'
);
modal.init();
//
const toolTip = new Tooltip("[data-tooltip]", "[data-mapa]");
toolTip.init();
//
const eventos = ["touchstart", "click"];
const dropDownMenu = new DropdownMenu("[data-dropdown]", eventos);
dropDownMenu.init();
//
const menuMobile = new MenuMobile(
  '[data-menu="button"]',
  '[data-menu="list"]',
  eventos
);
menuMobile.init();
//
initAnimaisApi();

const funciona = new Funcionamento("[data-semana]");
funciona.init();

initBtcDoacao();

const animaScrool = new AnimacaoScroll("[data-anime='scroll']");
animaScrool.init();

const slide = new SlideNav(".slide", ".slide-wrapper");
slide.init();
slide.addControl(".custom-controls");
