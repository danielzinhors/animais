export default class Funcionamento {
  constructor(funcionamento) {
    this.funcionamento = document.querySelector(funcionamento);
  }

  mostraFuncionamento(estaAberto, horarioAberto) {
    if (estaAberto && horarioAberto) {
      this.funcionamento.classList.add("aberto");
    } else {
      this.funcionamento.classList.remove("aberto");
    }
  }

  verificaFunionamento() {
    const diaSemana = this.funcionamento.dataset.semana.split(",").map(Number);
    const horarioSemana = this.funcionamento.dataset.horario
      .split(",")
      .map(Number);
    const dataAgora = new Date();
    const diaAgora = dataAgora.getDay();
    const horarioAgora = dataAgora.getUTCHours() - 3;
    const estaAberto = diaSemana.indexOf(diaAgora) !== -1;
    const horarioAberto = !!(
      horarioAgora >= horarioSemana[0] && horarioAgora < horarioSemana[1]
    );
    this.mostraFuncionamento(estaAberto, horarioAberto);
  }

  init() {
    if (this.funcionamento) {
      this.verificaFunionamento();
    }
    return this;
  }
}
