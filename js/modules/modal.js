export default class Modal {
  constructor(botaoAbrir, botaoFechar, containerModal) {
    this.botaoAbrir = document.querySelector(botaoAbrir);
    this.botaoFechar = document.querySelector(botaoFechar);
    this.containerModal = document.querySelector(containerModal);
    this.eventTogleModal = this.eventTogleModal.bind(this); // para pegar o this da classe ao invez do elemento no metodo
    this.cliqueForaModal = this.cliqueForaModal.bind(this);
  }

  togleModal() {
    this.containerModal.classList.toggle("ativo");
  }

  eventTogleModal(event) {
    event.preventDefault();
    this.togleModal();
  }

  cliqueForaModal(event) {
    event.preventDefault();
    if (event.target === this.containerModal) {
      this.togleModal(event);
    }
  }

  addEvent() {
    this.botaoAbrir.addEventListener("click", this.eventTogleModal);
    this.botaoFechar.addEventListener("click", this.eventTogleModal);
    this.containerModal.addEventListener("click", this.cliqueForaModal);
  }

  init() {
    if (this.botaoAbrir && this.botaoFechar && this.containerModal) {
      this.addEvent();
    }
    return this;
  }
}
