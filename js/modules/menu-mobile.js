import outsideClick from "./outsideclick.js";

export default class MenuMobile {
  constructor(menuButton, lista, eventos) {
    this.menuButton = document.querySelector(menuButton);
    this.list = document.querySelector(lista);
    if (eventos === undefined) this.events = ["touchstart", "click"];
    else this.events = eventos;
    this.classActive = "active";
    this.abreMenu = this.abreMenu.bind(this);
  }

  abreMenu(event) {
    event.preventDefault();
    this.menuButton.classList.add(this.classActive);
    this.list.classList.add(this.classActive);
    outsideClick(this.list, this.events, () => {
      this.list.classList.remove(this.classActive);
      this.menuButton.classList.remove(this.classActive);
    });
  }

  addEvent() {
    this.events.forEach((evento) => {
      this.menuButton.addEventListener(evento, this.abreMenu);
    });
  }

  init() {
    if (this.menuButton && this.list) {
      this.addEvent();
    }
    return this;
  }
}
