export default class AccordionFaq {
  constructor(list) {
    this.acordionList = document.querySelectorAll(list);
    this.classActive = "active";
    this.openAccordion = this.openAccordion.bind(this);
  }

  openAccordion(event) {
    event.currentTarget.classList.toggle(this.classActive);
    event.currentTarget.nextElementSibling.classList.toggle(this.classActive);
  }

  acitiveAcordion() {
    this.acordionList[0].classList.add(this.classActive);
    this.acordionList[0].nextElementSibling.classList.add(this.classActive);

    this.acordionList.forEach((item) => {
      item.addEventListener("click", this.openAccordion);
    });
  }

  init() {
    if (this.acordionList.length) {
      this.acitiveAcordion();
    }
    return this;
  }
}
