export default class AnimaNumber {
  constructor(divNum, observerTarget, obserClass) {
    this.numeros = document.querySelectorAll(divNum);
    this.observTarget = document.querySelector(observerTarget);
    this.observClass = obserClass;
    this.handleMutation = this.handleMutation.bind(this);
  }

  static incrementaNumeros(numero) {
    const total = +numero.innerText;
    const incremento = Math.floor(total / 10);
    let start = 0;
    const timer = setInterval(() => {
      start += incremento;
      numero.innerText = start;
      if (start > total) {
        numero.innerText = total;
        clearInterval(timer);
      }
    }, 215 * Math.random());
  }

  iniciaNumeros() {
    this.numeros.forEach((numero) =>
      this.constructor.incrementaNumeros(numero)
    );
  }

  handleMutation(mutation) {
    if (mutation[0].target.classList.contains(this.observClass)) {
      this.observer.disconnect();
      this.iniciaNumeros();
    }
  }

  addMutationObserver() {
    this.observer = new MutationObserver(this.handleMutation);
    this.observer.observe(this.observTarget, { attributes: true });
  }

  init() {
    if (this.numeros.length && this.observTarget) {
      this.addMutationObserver();
    }
    return this;
  }
}
