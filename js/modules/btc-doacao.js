export default function initBtcDoacao() {
  async function initBtc(url) {
    const spanBtc = document.querySelector(".btc-preco");
    if (spanBtc) {
      try {
        const bitcon = await await (await fetch(url)).json();
        const precoBtc = (100 / bitcon.BRL.sell).toFixed(4);
        spanBtc.innerHTML = precoBtc;
      } catch (erro) {
        console.log(erro);
      }
    }
  }

  initBtc("https://blockchain.info/ticker");
}
