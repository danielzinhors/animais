export default class Tooltip {
  constructor(dataTolltip, dataMapa) {
    this.tooltips = document.querySelectorAll(dataTolltip);
    this.mapa = document.querySelector(dataMapa);
    this.onMouseOver = this.onMouseOver.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  onMouseMove(event) {
    this.tooltipBox.style.top = `${event.pageY + 20}px`;
    const tamToolTipBox = this.tooltipBox.clientWidth;
    if (event.pageX - this.mapa.clientWidth > this.mapa.clientWidth) {
      this.tooltipBox.style.left = `${event.pageX - tamToolTipBox}px`;
    } else {
      this.tooltipBox.style.left = `${event.pageX + 20}px`;
    }
  }

  onMouseLeave({ currentTarget }) {
    this.tooltipBox.remove();
    currentTarget.removeEventListener("mouseleave", this.onMouseLeave);
    currentTarget.removeEventListener("mousemove", this.onMouseMove);
  }

  criarTooltipBox(element) {
    // cria tooltipbox e coloca no body
    const tooltipBox = document.createElement("div");
    const text = element.getAttribute("aria-label");
    tooltipBox.innerHTML = text;
    tooltipBox.classList.add("tooltip");
    document.body.appendChild(tooltipBox);
    this.tooltipBox = tooltipBox;
  }

  onMouseOver({ currentTarget }) {
    this.criarTooltipBox(currentTarget);
    currentTarget.addEventListener("mousemove", this.onMouseMove);
    currentTarget.addEventListener("mouseleave", this.onMouseLeave);
  }

  addEvent() {
    this.tooltips.forEach((item) => {
      item.addEventListener("mouseover", this.onMouseOver);
    });
  }

  init() {
    if (this.tooltips.length && this.mapa) {
      this.addEvent();
    }
    return this;
  }
}
