export default class TabMenu {
  constructor(dataSetMenu, dataSetContent) {
    this.tabMenu = document.querySelectorAll(dataSetMenu);
    this.tabContent = document.querySelectorAll(dataSetContent);
    this.classAtivo = "ativo";
  }

  activeTab(index) {
    this.tabContent.forEach((section) => {
      section.classList.remove(this.classAtivo);
    });
    this.tabContent[index].classList.add(
      this.classAtivo,
      this.tabContent[index].dataset.anime
    );
  }

  adicionaEvento() {
    this.tabContent[0].classList.add(this.classAtivo);
    this.tabMenu.forEach((itemMenu, index) => {
      itemMenu.addEventListener("click", () => {
        this.activeTab(index);
      });
    });
  }

  init() {
    if (this.tabMenu.length && this.tabContent.length) {
      this.adicionaEvento();
    }
    return this;
  }
}
