import outsideclick from "./outsideclick.js";

export default class DropdownMenu {
  constructor(dropDowns, eventos) {
    this.dropdownMenus = document.querySelectorAll(dropDowns);
    if (eventos === undefined) this.events = ["touchstart", "click"];
    else this.events = eventos;
    this.activeMenu = this.activeMenu.bind(this);
    this.activeClass = "active";
  }

  activeMenu(event) {
    event.preventDefault();
    const element = event.currentTarget;
    element.classList.add(this.activeClass);
    outsideclick(element, this.events, () => {
      element.classList.remove(this.activeClass);
    });
  }

  addEvent() {
    this.dropdownMenus.forEach((menu) => {
      this.events.forEach((userEvent) => {
        menu.addEventListener(userEvent, this.activeMenu);
      });
    });
  }

  init() {
    if (this.dropdownMenus.length) {
      this.addEvent();
    }
    return this;
  }
}
