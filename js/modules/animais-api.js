import AnimaNumer from "./anima-numeros.js";

export default function initAnimaisApi() {
  function createAnimal(animal) {
    const div = document.createElement("div");
    div.classList.add("numero-animal");
    div.innerHTML = `<h3>${animal.animal}</h3><span data-numero>${animal.qtd}</span>`;
    return div;
  }

  async function fecthAnimais(url) {
    const numerosGrid = document.querySelector(".numeros-grid");
    if (numerosGrid) {
      try {
        const animais = await fetch(url);
        const animaisJson = await animais.json();
        animaisJson.forEach((element) => {
          numerosGrid.appendChild(createAnimal(element));
        });
        const animaNum = new AnimaNumer("[data-numero]", ".numeros", "ativo");
        animaNum.init();
      } catch (erro) {
        console.log(erro);
      }
    }
  }

  fecthAnimais("./animais.json");
}
